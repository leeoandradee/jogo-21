package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {

    private List<Historico> historico = new ArrayList<>();

    public List<Historico> getHistorico() {
        return historico;
    }

    public void setHistorico(List<Historico> historico) {
        this.historico = historico;
    }
}
