package br.com.itau;

import java.util.Date;

public class Historico {

    private int pontuacao;
    private String data;


    public Historico(int pontuacao, String data) {
        this.pontuacao = pontuacao;
        this.data = data;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
