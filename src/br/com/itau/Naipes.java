package br.com.itau;

public enum Naipes {

    PAUS("paus"),
    OUROS("ouros"),
    COPAS("copas"),
    ESPADAS("espadas");

    private String nomeNaipe;

    Naipes(String nomeNaipe) {
        this.nomeNaipe = nomeNaipe;
    }
}
