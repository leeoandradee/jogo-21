package br.com.itau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baralho {

    private List<Cartas> cartas = new ArrayList<>();

    public Baralho() {
        for (Cartas carta: Cartas.values()) {
            for (Naipes naipe: Naipes.values()) {
                carta.setNaipes(naipe);
                cartas.add(carta);
            }
        }
        Collections.shuffle(cartas);

        System.out.println(cartas.toString());
    }

    public List<Cartas> getCartas() {
        return cartas;
    }

    public void setCartas(List<Cartas> cartas) {
        this.cartas = cartas;
    }
}
