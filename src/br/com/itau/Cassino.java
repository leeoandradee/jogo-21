package br.com.itau;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cassino {

    private static Jogador jogador;

    public static void mostrarMenu() {
        jogador = new Jogador();
        int opcao;
        do {
            String opcaoMenu = JOptionPane.showInputDialog(null, "\n----------WELCOME TO THE BLACKJACK--------"
                    + "\n\n  1 - Jogar"
                    + "\n 2 - Mostrar melhores pontuações"
                    + "\n 3 - Sair"
                    + "\n\n -----------------------"
            );
            opcao = Integer.parseInt(opcaoMenu);
            if (opcao == 1) {
                jogar();
            } else if (opcao == 2) {
                mostrarPontuacoes();
            } else if (opcao > 3) {
                JOptionPane.showMessageDialog(null, "Ops, a opção escolhida é inválida.");
            }
        } while(opcao != 3);
    }

    public static void jogar() {
        int opcao;
        int pontuacao = 0;
        String msgJogo = "";
        Baralho baralho = new Baralho();

        do {
            String opcaoMenu = JOptionPane.showInputDialog(null, "\n 1 - Sacar carta \n 2 - Parar");
            opcao = Integer.parseInt(opcaoMenu);
            if (opcao == 1) {
                Cartas carta = Carteador.sacarCarta(baralho);
                pontuacao = pontuacao + carta.getValorCarta();
                if (pontuacao > 21) {
                    msgJogo = "\nQue pena, você perdeu :(\n";
                    opcao = 2;
                } else if (pontuacao == 21) {
                    msgJogo = "\nVocê ganhooou :)\n";
                    salvarPontuacao(pontuacao);
                    opcao = 2;
                }
                JOptionPane.showConfirmDialog(null ,
                         msgJogo + "\nCarta sacada: "
                        + carta.getNomeCarta()
                        + " " + carta.getNaipes().toString()
                        + "\n\nPontuação total: " + pontuacao + "\n ");
            } else if (opcao == 2) {
                JOptionPane.showConfirmDialog(null, "Sua pontuação foi: " + pontuacao);
                if(pontuacao <= 21) {
                    salvarPontuacao(pontuacao);
                }
            }

        } while (opcao != 2);
    }

    public static void mostrarPontuacoes() {
        if (jogador != null && jogador.getHistorico().size() > 0) {
            String pontuacoes = "\n Histórico de pontuações:\n";
            for (Historico historico: jogador.getHistorico()) {
                pontuacoes = pontuacoes + "\n" + historico.getData() + " - " + historico.getPontuacao() + " pontos";
            }
            JOptionPane.showConfirmDialog(null, pontuacoes);
        } else {
            JOptionPane.showConfirmDialog(null, "Você ainda não possui nenhum histórico.");
        }
    }

    public static void salvarPontuacao(int pontuacao) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Historico historico = new Historico(pontuacao, formatter.format(date));
        jogador.getHistorico().add(historico);
    }


}
